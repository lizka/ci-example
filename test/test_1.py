#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from subprocess import check_output

PATH_TO_BIN = './ci-example.elf'

def test_numbers_1_1():
    numbers = '1 1'
    result = '2'
    cmd = 'echo {} | {}'.format(numbers, PATH_TO_BIN)
    output = check_output(cmd, shell=True).decode()[:-1]
    assert output == result

def test_numbers_minus1_1():
    numbers = '-1 1'
    result = '0'
    cmd = 'echo {} | {}'.format(numbers, PATH_TO_BIN)
    output = check_output(cmd, shell=True).decode()[:-1]
    assert output == result

def test_numbers_minus1_0():
    numbers = '-1 0'
    result = '-1'
    cmd = 'echo {} | {}'.format(numbers, PATH_TO_BIN)
    output = check_output(cmd, shell=True).decode()[:-1]
    assert output == result
