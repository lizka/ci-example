#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from subprocess import check_output

PATH_TO_BIN = './ci-example.elf'

def test_numbers_2048_2048():
    numbers = '2048 2048'
    result = '4096'
    cmd = 'echo {} | {}'.format(numbers, PATH_TO_BIN)
    output = check_output(cmd, shell=True).decode()[:-1]
    assert output == result

def test_numbers_minus2048_2048():
    numbers = '-2048 2048'
    result = '0'
    cmd = 'echo {} | {}'.format(numbers, PATH_TO_BIN)
    output = check_output(cmd, shell=True).decode()[:-1]
    assert output == result

def test_numbers_minus2048_4096():
    numbers = '-2048 4096'
    result = '2048'
    cmd = 'echo {} | {}'.format(numbers, PATH_TO_BIN)
    output = check_output(cmd, shell=True).decode()[:-1]
    assert output == result
